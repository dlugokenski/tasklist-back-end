package business;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.inject.Inject;

import org.json.JSONObject;

import com.google.gson.Gson;

import dao.TaskListDAO;
import entity.Atividade;

@Stateful
public class TaskListBusiness extends DefaultBusiness<Atividade> implements Serializable {
	
	@Inject
	private TaskListDAO taskListDAO;

//	public JSONObject buscarCarteiraAtual(long idUsuario, int numeroCarteira){	
//		try{
//			return percorrerMontarCarteiraAtual(carteiraDAO.buscarCarteira(idUsuario, numeroCarteira));
//		} catch (Exception e) {
//			return StringUtil.retornarErro("Ocorreu um erro recuperar os dados da carteira!");
//		}
//	}
	
	@Override
	public Object pesquisarByID(Long identificador) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getAtivos() throws Exception {
		return taskListDAO.findAll();
	}

	@Override
	public List<?> cadastrarList() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object cadastrarObject(JSONObject object) throws Exception {
		Gson gson = new Gson();
		Atividade atividade = gson.fromJson(object.getJSONObject("objeto").toString(), Atividade.class);
		Atividade newAtividade = taskListDAO.save(atividade);
		return newAtividade;
	}
}
