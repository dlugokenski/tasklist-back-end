package business;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.json.JSONObject;


public abstract class DefaultBusiness<E> {

	public abstract Object pesquisarByID(Long identificador) throws Exception;

	public abstract List<?> getAtivos() throws Exception;
	
	public abstract List<?> cadastrarList() throws Exception;
	
	public abstract Object cadastrarObject(JSONObject object) throws Exception;
	
	
}
