DROP TABLE IF EXISTS atividade;
CREATE TABLE atividade(
	id SERIAL PRIMARY KEY,
	title VARCHAR,
	descricao VARCHAR(50),
	status BOOLEAN,
	data_criacao DATE,
	data_edicao DATE,
	data_remocao DATE,
	data_conclusao DATE
)