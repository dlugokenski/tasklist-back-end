package dao;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.AbstractEntity;

public abstract class AbstractPersistence<T extends AbstractEntity, PK extends Number> {

	@Inject
	private Aplication aplication;
	
	private Class<T> entityClass;

	public AbstractPersistence(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public T save(T e) {	
		if (e.getId() != 0)
			return getEntityManager().merge(e);
		else {
			getEntityManager().persist(e);
			return e;
		}
	}

	public void remove(T entity) {
		getEntityManager().remove(getEntityManager().merge(entity));
	}

	public T find(PK id) {
		return getEntityManager().find(entityClass, id);
	}

	public List<T> findAll() {
		CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}

	public List<T> findRange(int[] range) {
		CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		Query q = getEntityManager().createQuery(cq);
		q.setMaxResults(range[1] - range[0]);
		q.setFirstResult(range[0]);
		return q.getResultList();
	}

	public int count() {
		CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
		Root<T> rt = cq.from(entityClass);
		cq.select(getEntityManager().getCriteriaBuilder().count(rt));
		Query q = getEntityManager().createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}
	
	public String findString(String queryString, Map<String, ?> parametros) {
		Query query = getEntityManager().createNativeQuery(queryString);		
		for (Map.Entry<String, ?> entry : parametros.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}			
		String result = (String) query.getSingleResult();
		return result;
	}
	
	public boolean findBoolean(String queryString, Map<String, ?> parametros) {
		Query query = getEntityManager().createNativeQuery(queryString);
		for (Map.Entry<String, ?> entry : parametros.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}			
		boolean result =  (Boolean) query.getSingleResult();
		return result;
	}
	

	public  EntityManager getEntityManager() {
		return aplication.getEntityManager();
	}

}
