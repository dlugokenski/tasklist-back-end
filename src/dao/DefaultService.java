package dao;

import java.util.List;

import javax.ejb.Local;

import org.apache.poi.ss.formula.functions.T;

@Local
public interface DefaultService<T>{

	public T save(T t);
	
	public void remove(T t);
	
	public T find(Long id);
	
	public List<T> findAll();

}
