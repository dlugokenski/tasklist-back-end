package dao;

import javax.enterprise.context.Dependent;

import entity.Atividade;

@Dependent
public class TaskListDAO extends AbstractPersistence<Atividade, Long> implements DefaultService<Atividade>{
	
	public TaskListDAO() {
		super(Atividade.class);
	}

}
