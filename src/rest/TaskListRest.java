package rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;

import business.DefaultBusiness;
import business.TaskListBusiness;
import entity.Atividade;

@RequestScoped
@Path("/rest/taskList")
public class TaskListRest extends DefaultRestService<Atividade>{

	@Inject
    private TaskListBusiness taskListBusiness;
	
	
	@Override
	DefaultBusiness<Atividade> getBusiness() throws Exception {
		return taskListBusiness;
	}
}
