package rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import business.DefaultBusiness;

public abstract class DefaultRestService<E>{

	abstract DefaultBusiness<E> getBusiness() throws Exception;
	
	@POST
	@GET
	@Path("/cadastrar")
//	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response Object(JSONObject object) throws Exception {
		Object objetoReturn = getBusiness().cadastrarObject(object);
		return enviarResposta(objetoReturn);
	}
	

	@GET
	@POST
	@Path("/id/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByID(@PathParam("id") String id) {
//		try {
//			Long identificador = toLong(id);
//			Object dto = getBusiness().pesquisarByID(identificador);
//			return enviarResposta(dto);
//		} catch (Exception e) {
//			return error(e);
//		}
		return enviarResposta(null);
	}
	
	@GET
	@Path("/consultar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAtivos() {
		try {
			Object dto = getBusiness().getAtivos();
			return enviarResposta(dto);
		} catch (Exception e) {
			return null;
		}
	}


	@Produces(MediaType.APPLICATION_JSON)
	public Response enviarResposta(JSONObject jsonObject) {
		return Response.status(200).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209600").entity(jsonObject.toString()).build();
	}
	
	@Produces(MediaType.APPLICATION_JSON)
	public Response enviarResposta(Object object) {
		return Response.status(200).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209600").entity(object).build();
	}

}
